import React from 'react';

import Subjects from '../components/subjects';
import { Row, Col } from 'antd';

import axios from 'axios';


const filter_year = (year, array) => {
	return array.filter( (el) => {
		return el.year === year;
	})
}



class SubjectList extends React.Component {

	state = {
		subjects1: [],
		subjects2: [],
		subjects3: [],
		subjects4: [],
		subjects5: []
	}

	componentDidMount() {
		axios.get('http://127.0.0.1:8000/carrer/listSubjects/', { 'headers': {Authorization: 'token d518f3d6163fd92f074414055330022bd26fcc5a'}})
			.then( res => {
				this.setState({
					subjects1: filter_year(1, res.data),
					subjects2: filter_year(2, res.data),
					subjects3: filter_year(3, res.data),
					subjects4: filter_year(4, res.data),
					subjects5: filter_year(5, res.data),
				});
			}
		);
	}
		

	render () {
		return (
			<h1>Welcome</h1>
				
		)
	}
};

export default SubjectList;
