import React from 'react';
import { Route } from 'react-router-dom';

import SubjectList from './containers/SubjectsLists';
import SubjectDetail from './containers/SubjectDetails.js';
import carrerYear from './containers/carrerYear.js';

const BaseRouter = () => (
	<div>
		<Route exact path='/' component={SubjectList} />
		<Route exact path='/:Anio' component={carrerYear} />
		<Route exact path='/materia/:SubjectID' component={SubjectDetail} />
	</div>
);

export default BaseRouter;

//<Route exact path='/:SubjectID' component={SubjectDetail} />
